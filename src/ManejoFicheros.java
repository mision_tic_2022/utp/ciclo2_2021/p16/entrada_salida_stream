import java.io.PrintWriter;

public class ManejoFicheros {
    public static void main(String[] args) {

        try (PrintWriter salida = new PrintWriter("./numeros.txt")){
            // Matriz de números enteros
            int[][] numeros = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 } };

            //var ruta_archivo = "./numeros.txt";
            // Crear objeto de salida
            //PrintWriter salida = new PrintWriter(ruta_archivo);
            //Recorrer la matriz de números
            for(int i = 0; i < numeros.length; i++){
                //Iterar los arreglos hijos
                for(int j = 0; j < numeros[i].length; j++){
                    salida.print(numeros[i][j] + ", ");
                }
                salida.println("");
            }
            //salida.close();
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println("Error en la salida de datos");
        }

    }
}
