import java.io.IOException;

public class IOEstandar {
    public static void main(String[] args) throws Exception{
        //Reservamos un espacio en memoria para almacenar un flujo de datos
        byte[] buffer = new byte[255];
        System.out.println("Escriba un texto: ");
        try {
            //Entrada de datos que es guardado directamente en el buffer
            System.in.read(buffer, 0, 255);
        } catch (IOException e) {//Excepción que genera el flujo de datos
            //TODO: handle exception
            System.out.println("Error");
        }
        //Da salida al buffer de entrada
        System.out.println("El texto escrito es: "+new String(buffer));

    }
}
