import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;


public class EntradaSalida {

    public static void main(String[] args){

        //Objeto para la entrada de datos
        BufferedReader entrada = new BufferedReader( new InputStreamReader(System.in) );
        //Objeto para la manipulaciòn de salida de datos
        PrintWriter salida = new PrintWriter(System.out, true);
        //Salida de datos
        salida.println("Escriba un texto: ");
        String texto;
        try {
            texto = entrada.readLine();
            //Mostrar lo digitado por el usuario
            salida.println("El usuario digitó: "+texto);
        } catch (Exception e) {
            //TODO: handle exception
            System.err.println("Error");
        }
        

    }
    
}
