import java.io.IOException;

public class App {
    public static void main(String[] args) throws Exception {
        //Variable que me representa la cantidad de bytes
        
        int numBytes = 0;
        char caracter;
        System.out.println("Escribe un texto: ");
        
        //try-catch -> Manejo de excepciones 
        try {
            //Iterar hasta que detecte un salto de linea (enter)
            do{
                //Entrada de datos caracteres
                caracter = (char) System.in.read();
                System.out.println(caracter);
                numBytes++;
            }while(caracter != '\n');
            System.err.printf("%d bytes leidos %n", numBytes);
        } catch (IOException e) {
            //TODO: handle exception
            System.out.println("Error");
        }
    
    }
}

/********************************************************************************
 * https://gitlab.com/mision_tic_2022/utp/ciclo2_2021/p16/entrada_salida_stream
 * 
 * https://gitlab.com/mision_tic_2022/utp/ciclo2_2021/p16
 *********************************************************************************/
